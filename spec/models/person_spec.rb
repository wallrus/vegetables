require 'spec_helper'

describe Car do
  it "validates presence of first name" do
    is_expected.to validate_presence_of(:first_name)
  end
end