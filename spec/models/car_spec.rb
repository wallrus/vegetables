require 'spec_helper'

RSpec.describe Car, type: :model do
  it "validates presence of registration number" do
    is_expected.to validate_presence_of(:registration_number)
  end
  
  it "validates presence of model" do
    is_expected.to validate_presence_of(:model)
  end
  
  it "validates presence of owner" do
    is_expected.to validate_presence_of(:owner_id)
  end
end