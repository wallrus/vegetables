require 'rails_helper'

describe place_rent do
  it "validates presence of start at" do
    is_expected.to validate_presence_of(:starts_at)
  end
  
  it "validates presence of ends at" do
    is_expected.to validate_presence_of(:ends_at)
  end
  
  it "validates presence of car" do
    is_expected.to validate_presence_of(:car_id)
  end
end