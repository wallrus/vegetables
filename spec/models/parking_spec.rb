require 'spec_helper'

RSpec.describe Parking, type: :model do
  it "validates presence of places" do
    is_expected.to validate_presence_of(:places)
  end
  
  it "validates presence of owner" do
    is_expected.to validate_presence_of(:owner_id)
  end
  
  it "validates presence of address" do
    is_expected.to validate_presence_of(:address_id)
  end
  
  it "validates numericality of hour price" do
    is_expected.to validate_numericality_of(:hour_price)
  end
  
  it "validates numericality of day price" do
    is_expected.to validate_numericality_of(:day_price)
  end
  
  it "validates numericality of places" do
    is_expected.to validate_numericality_of(:places).only_integer
  end
  
  it "validates certain kinds of day prices are allowed" do
    is_expected.to validate_inclusion_of(:day_price.kind).in_array(["outdoor", "indoor", "private", "street"])
  end
  
end