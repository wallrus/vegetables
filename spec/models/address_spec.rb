require 'spec_helper'

describe Address do
  it "validates presence of city" do
    is_expected.to validate_presence_of(:city)
  end
  
  it "validates presence of zip_code" do
    is_expected.to validate_presence_of(:zip_code)
  end
  
  it "validates presence of street" do
    is_expected.to validate_presence_of(:street)
  end
end