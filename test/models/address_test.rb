require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  def setup
    @address = Address.new(city: "Hamburglar", zip_code: "two", street: "Banana")
  end

  test "should be valid" do
    assert @address.valid?
  end
  
  test "assert city's presence" do
    @address.city = " "
    assert_not @address.valid?
  end
  
  test "assert zip code's presence" do
    @address.zip_code = " "
    assert_not @address.valid?
  end
  
  test "assert street's presence" do
    @address.street = " "
    assert_not @address.valid?
  end
end
