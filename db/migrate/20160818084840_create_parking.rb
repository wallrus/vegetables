class CreateParking < ActiveRecord::Migration
  def change
    create_table :parkings do |t|
      t.string :places
      t.string :kind
      t.string :hour_price
      t.string :day_price
      t.string :address_id
      t.string :owner_id
    end
  end
end
