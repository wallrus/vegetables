class CreatePlaceRents < ActiveRecord::Migration
  def change
    create_table :place_rents do |t|
      t.string :starts_at
      t.string :ends_at
      t.string :car_id
      t.string :parking_id

      t.timestamps null: false
    end
  end
end
