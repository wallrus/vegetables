class Address < ActiveRecord::Base
  belongs_to :parking
  
  validates :city, presence: true
  validates :street, presence: true
  validates :zip_code, presence: true
end