class Parking < ActiveRecord::Base
  has_one :address
  has_many :place_rent
  
  validates :places, presence: true, numericality: { only_integer: true }
  validates :address_id, :owner_id, presence: true
  validates :hour_price, :day_price, numericality: true
  validates :day_price.kind, inclusion: { in: ["outdoor", "indoor", "private", "street"] }
end
