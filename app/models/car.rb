class Car < ActiveRecord::Base
  belongs_to :person
  
  validates :registration_number, :model, :owner_id, presence: true
end
