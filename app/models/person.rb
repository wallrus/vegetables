class Person < ActiveRecord::Base
  has_many :cars
  
  validates :first_name, presence: true
end