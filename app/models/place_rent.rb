class PlaceRent < ActiveRecord::Base
  belongs_to :parking
  has_one :car
  
  validates :star, :end_date, :car, presence: true
end
